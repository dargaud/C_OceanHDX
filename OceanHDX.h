#ifndef __TB_NEWPORT_H
#define __TB_NEWPORT_H

#include <time.h>

extern int    OceanHDX_InitEth(char* Address, int Port);	// Default port should be 57357
extern int    OceanHDX_InitUSB(int VID, int PID);			// Use lsusb to get the VID and PID
extern int    OceanHDX_Close(void);

enum { D_ALL, D_NO_REPLY, D_NONE };			// Verbosity level for OceanHDX_Debug(): display cmd&reply, just cmd or neither
extern void   OceanHDX_Debug(int Display);
extern char*  OceanHDX_LastErrMsg(void);

extern int    OceanHDX_Reset(void);
extern int    OceanHDX_ResetRestore(void);
extern int    OceanHDX_GetRecoveryStatus(void);

extern char*  OceanHDX_GetFirmware(void);	// Calls several other functions
extern char*  OceanHDX_GetSerial(void);
extern char*  OceanHDX_GetAlias(void);
extern int    OceanHDX_SetAlias(char *Alias);

extern char*  OceanHDX_GetUsbInfo(void);	// Calls several other functions

extern int    OceanHDX_GetUserStringCount(void);
extern int    OceanHDX_GetUserStringLen(int Idx);
extern char*  OceanHDX_GetUserString(int Idx);
extern int    OceanHDX_SetUserString(int Idx, char* Str);
	
extern time_t OceanHDX_GetCurrentTime(void);
extern int    OceanHDX_SetCurrentTime(void);
	
extern int    OceanHDX_GetTestPayload(unsigned int Len);
extern int    OceanHDX_EchoPayload(unsigned char* Payload, unsigned int Len);
extern int    OceanHDX_Abort(void);

extern int    OceanHDX_GetBufEnabled(void);
extern int    OceanHDX_GetMaxBufSize(void);
extern int    OceanHDX_GetCurrentBufSize(void);
extern int    OceanHDX_ClearBuffer(void);
extern int    OceanHDX_RemoveOldestSpectra(unsigned int Nb);
extern int    OceanHDX_SetCurrentBufSize(unsigned int Size);
extern int    OceanHDX_GetNbSpectra(void);

extern int    OceanHDX_GetScansToAverage(void);
extern int    OceanHDX_SetScansToAverage(unsigned short Avg);
extern int    OceanHDX_GetMaxAdcCounts(void);

extern int    OceanHDX_GetIntegrationTime(void);
extern int    OceanHDX_SetIntegrationTime(unsigned int Intg);
extern int    OceanHDX_GetMinIntegrationTime(void);
extern int    OceanHDX_GetMaxIntegrationTime(void);
extern int    OceanHDX_GetIntegrationTimeStepSize(void);

extern int    OceanHDX_GetTriggerMode(void);
extern int    OceanHDX_SetTriggerMode(int TriggerMode);
extern int    OceanHDX_GetNbSpectraPerTrig(void);
extern int    OceanHDX_SetNbSpectraPerTrig(unsigned int Nb);
	
extern int    OceanHDX_GetLampEnable(void);
extern int    OceanHDX_SetLampEnable(int TriggerMode);

extern int    OceanHDX_GetAcqDelay(void);
extern int    OceanHDX_SetAcqDelay(unsigned int Intg);
extern int    OceanHDX_GetMinAcqDelay(void);
extern int    OceanHDX_GetMaxAcqDelay(void);
extern int    OceanHDX_GetAcqDelayStepSize(void);

extern int    OceanHDX_GetNbOfTemperatures(void);
extern float  OceanHDX_GetTemperature(int Index);
extern float* OceanHDX_GetAllTemperatures(void);
	
/////////////////////// Spectrum-related functions ////////////////////////////

#define PIXEL_NB 2048
#define SIZE_OF_DATA ((PIXEL_NB+20)*2)								// 4136 bytes, including unused, dark and bevel
#define SIZE_OF_RAW (sizeof(tMetadata)+SIZE_OF_DATA+sizeof(int))	// 4204 bytes, including metadata and checksum

typedef struct sSpectroPixels {	// There are 2068 pixels in a spectrum, but not all are usable
	unsigned short Unusable, Dark[3], Bevel[6], Spectrum[2048], Bevel2[6], Dark2[4];
} tSpectroPixels;	// This applies to the MAYA2000 but I suspect it is the same here

// The pixel intensities are corrected for temperature drift and fixed-pattern noise.
extern tSpectroPixels* OceanHDX_GetImmediateSpectrum(int *Size);
// Uncorrected raw spectra
extern tSpectroPixels* OceanHDX_GetRawSpectrumWithMetaData(int Nb, int *Size);


typedef struct sOceanHDX_Coefs {
	float Coefs[16];
	int NB;
} tOceanHDX_Coefs;
// Do not change those values directly, use provided functions
extern tOceanHDX_Coefs OceanHDX_Wavelength, OceanHDX_Nonlinearity, OceanHDX_StrayLight;

extern double OceanHDX_Wavelengths[PIXEL_NB];	// Array of wavelengths for each pixel. 
												// Computed once by Lambda() below as soon as we have read the wavelength coefficients in OceanHDX_GetCoefs()

////////////////////// Coefficient-related functions //////////////////////////

extern int    OceanHDX_GetWavelengthCoefCount(void);
extern int    OceanHDX_GetNonlinearityCoefCount(void);
extern int    OceanHDX_GetStrayLightCoefCount(void);
extern float  OceanHDX_GetWavelengthCoef(unsigned char Index);
extern float  OceanHDX_GetNonlinearityCoef(unsigned char Index);
extern float  OceanHDX_GetStrayLightCoef(unsigned char Index);
extern int    OceanHDX_GetCoefs(void);												// Calls the above functions
extern int    OceanHDX_SetWavelengthCoef(unsigned char Index, float Coef);
extern int    OceanHDX_SetWavelengthCoefs(float I, float C1, float C2, float C3);	// Calls the above function

////////////////// Data processing functions (non-firmware) ///////////////////

extern int    OceanHDX_CorrectForNonLinearities(tSpectroPixels* S, int Nb, double Corrected[], unsigned short *Max);
extern void   OceanHDX_DarkRemoval(tSpectroPixels* Spectro, tSpectroPixels *Dark);
extern int    OceanHDX_BoxcarAvg(tSpectroPixels* P, int Nb);
extern int    OceanHDX_SaveArray(int DateTime, char* TagName, tSpectroPixels* P, int Size, int Nb, int Intg);

#endif
