#include "ColorScale.h"
#include <analysis.h>
#include <ansi_c.h>
#include <iso646.h>

#include <cvirte.h>		
#include <userint.h>
#include <toolbox.h>

#include "SpectroVisu.h"

static int Pnl, PlotC=0, PlotI=0 /*, PlotIntr=0*/;
static int Disp=2;				// Display intensity, corrected or both
static int Lines=0;				// Draw emission lines on rainbow

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Distribute the top controls so they don't overlap
///////////////////////////////////////////////////////////////////////////////
static void Distribute(void) {
	#define GETLW(Ctrl) GetCtrlAttribute (Pnl, (Ctrl), ATTR_LABEL_LEFT,  &Left);\
					  GetCtrlAttribute (Pnl, (Ctrl), ATTR_LABEL_WIDTH, &Width)
	int Left, Width;
	                                                                GETLW(PNL_DISPLAY);
	SetCtrlAttribute(Pnl, PNL_LOG,        ATTR_LEFT, Left+Width+3); GETLW(PNL_LOG);
	SetCtrlAttribute(Pnl, PNL_LINES,      ATTR_LEFT, Left+Width+3); GETLW(PNL_LINES);
	SetCtrlAttribute(Pnl, PNL_SATURATION, ATTR_LEFT, Left+Width+3); GETLW(PNL_SATURATION);
	SetCtrlAttribute(Pnl, PNL_PEAK,       ATTR_LEFT, Left+Width+3);	GETLW(PNL_PEAK);
	SetCtrlAttribute(Pnl, PNL_X,          ATTR_LEFT, Left+Width+3); GETLW(PNL_X);
	SetCtrlAttribute(Pnl, PNL_Y,          ATTR_LEFT, Left+Width+3);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Convert a wavelength to an approximate color
/// HIFN	Taken from Earl F. Glynn's web page:
/// HIFN	http://www.efg2.com/Lab/ScienceAndEngineering/Spectra.htm
/// HIPAR	NM/Wavelength in nm
/// HIRET	24-bit color
///////////////////////////////////////////////////////////////////////////////
static int WavelengthToRGB(double NM) {
	const double Gamma = 0.80;
	const  double IntensityMax = 255.;
	double factor;
	double Red, Green, Blue;
	#define InfSup(x) (x) and (x)
	if (380<=InfSup(NM)<440) {
		Red=-(NM-440)/(440-380); Green=0.0;                 Blue=1.0;
	} else if (440<=InfSup(NM)<490) {
		Red=0.0;                 Green=(NM-440)/(490-440);  Blue=1.0;
	} else if (490<=InfSup(NM)<510) {
		Red=0.0;                 Green=1.0;                 Blue=-(NM-510)/(510-490);
	} else if (510<=InfSup(NM)<580) {
		Red=(NM-510)/(580-510);  Green=1.0;                 Blue=0.0;
	} else if (580<=InfSup(NM)<645) {
		Red=1.0;                 Green=-(NM-645)/(645-580); Blue=0.0;
	} else if (645<=InfSup(NM)<781) {
		Red=1.0;                 Green=0.0;                 Blue=0.0;
	} else {
		Red=0.0;                 Green=0.0;                 Blue=0.0;
	};
	
	// Let the intensity fall off near the vision limits
	factor=	380<=InfSup(NM)<420 ? 0.3 + 0.7*(NM - 380) / (420 - 380) :
			420<=InfSup(NM)<701 ? 1.0 :
			701<=InfSup(NM)<781 ? 0.3 + 0.7*(780 - NM) / (780 - 700) :
			0.0;
	
	#define ROUND(x) ((int)((x)+0.5))
	return	// Don't want 0^x = 1 for x <> 0
		((Red  ==0.0 ? 0 : ROUND(IntensityMax * pow(Red   * factor, Gamma)))<<16) |
		((Green==0.0 ? 0 : ROUND(IntensityMax * pow(Green * factor, Gamma)))<< 8) |
		((Blue ==0.0 ? 0 : ROUND(IntensityMax * pow(Blue  * factor, Gamma)))<< 0);
}

///////////////////////////////////////////////////////////////////////////////
// Contains a scan with its wavelengths, intensities, and corrections for non-linearities
#define NB_PIX 2048
typedef struct sScan {
	double Nm[NB_PIX], Intensity[NB_PIX], Corrected[NB_PIX];
} tScan;
static tScan Scan={0};	// Yeah, I know I use a global... for such a small program...

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Perform a gaussian fit to estimate the real peak near the index
/// HIPAR	iMax/Index of the peak
/// HIRET	NM position of the interpolated peak
///////////////////////////////////////////////////////////////////////////////
double XOfPeak(int iMax) {
	#define NB 11	// Look at the NB points around the peak itself and interpolate this area
	ssize_t Count;
	double *PeakLocations, *PeakAmplitudes, *Peak2nd;
	double ErrEst, PreciseNm;
	static double Ramp[NB]={0}; 
	if (Ramp[1]!=1.) for (int i=0; i<NB; i++) Ramp[i]=i;
	if (iMax<NB or iMax>NB_PIX-NB) return 0;
	PeakDetector (&Scan.Corrected[iMax]-NB/2, NB, 0.0, 3, DETECT_PEAKS, ANALYSIS_TRUE,
				  ANALYSIS_TRUE, &Count, &PeakLocations, &PeakAmplitudes, &Peak2nd);
	PolyInterp (Ramp, &Scan.Nm[iMax]-NB/2, NB, PeakLocations[0], &PreciseNm, &ErrEst);
	FreeAnalysisMem(PeakLocations);
	FreeAnalysisMem(PeakAmplitudes);
	FreeAnalysisMem(Peak2nd);
	return PreciseNm;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Plot a series of colors at the appropriate wavelengths
///////////////////////////////////////////////////////////////////////////////
void PlotRainbow(int Min, int Max, double MinInt, double MaxInt, double *Intensity) {
	int i, Color;
	for (i=0; i<NB_PIX-1; i++) {
		Color=WavelengthToRGB(Scan.Nm[i]);
		if (Lines) {
			double Ratio=(Intensity[i]-MinInt)/(MaxInt-MinInt); 
			Color=ColorScaleRGB (Color, VAL_WHITE, sqrt(Ratio));
		}
		if (Color) PlotRectangle (Pnl, PNL_GRAPH, Scan.Nm[i], 0/*Min*/, Scan.Nm[i+1], Max, Color, Color);
	}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read a scan from a file. Will not process RAW files with multiple scans
/// HIRET	0 if no error
///////////////////////////////////////////////////////////////////////////////
static int ProcessFile(char* PathName) {
	char FileName[MAX_FILENAME_LEN], Str[256], Title[MAX_FILENAME_LEN+20];
	int i, NbSaturation=0;
	SplitPath (PathName, NULL, NULL, FileName);
	sprintf(Title, "OceanHDX spectrogram: %s", FileName);
	SetPanelAttribute (Pnl, ATTR_TITLE, Title);
	FILE *fd=fopen(PathName, "r");
	if (fd==NULL) { MessagePopup("File read error!", strerror(errno)); return errno; }
	
	// Skip header
	for (i=0; i<11; i++) fgets (Str, 256, fd);	
	
	for (i=0; i<NB_PIX; i++) { 
		fscanf(fd, "%lf\t%lf\t%lf\n", &Scan.Nm[i], &Scan.Intensity[i], &Scan.Corrected[i]);
		if (Scan.Intensity[i]==0xFFFF) NbSaturation++;
	}	
	fclose(fd);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Display the Scan arrays
///////////////////////////////////////////////////////////////////////////////
static void DisplaySpectrum(void) {
	char Str[256];
	int i, NbSaturation=0;
	double Min=PositiveInfinity(), Max=NegativeInfinity();
	int iMin=INT_MAX, iMax=INT_MIN;
	for (i=0; i<NB_PIX; i++) { 
		if (Scan.Corrected[i]<Min) Min=Scan.Corrected[iMin=i];
		if (Scan.Corrected[i]>Max) Max=Scan.Corrected[iMax=i];
		if (Scan.Intensity[i]<Min) Min=Scan.Intensity[iMin=i];
		if (Scan.Intensity[i]>Max) Max=Scan.Intensity[iMax=i];
		if (Scan.Intensity[i]==0xFFFF) NbSaturation++;
	}	

	SetCtrlAttribute(Pnl, PNL_X, ATTR_VISIBLE, 0);
	SetCtrlAttribute(Pnl, PNL_Y, ATTR_VISIBLE, 0);

	SetCtrlAttribute   (Pnl, PNL_GRAPH, ATTR_REFRESH_GRAPH, 0);
	DeleteGraphPlot    (Pnl, PNL_GRAPH, -1, VAL_DELAYED_DRAW);
	DeleteGraphAnnotation (Pnl, PNL_GRAPH, -1);

	SetAxisScalingMode (Pnl, PNL_GRAPH, VAL_BOTTOM_XAXIS, VAL_MANUAL, floor(Scan.Nm[0]), ceil(Scan.Nm[NB_PIX-1]));
	SetAxisScalingMode (Pnl, PNL_GRAPH, VAL_LEFT_YAXIS,   VAL_MANUAL, 0.0, Max==0?1:Max);
	
	PlotRainbow(Scan.Corrected[iMin], Scan.Corrected[iMax], Min, Max, Disp&2 ? Scan.Corrected : Scan.Intensity);
	
	PlotC=PlotXY(Pnl, PNL_GRAPH, Scan.Nm, Scan.Corrected, NB_PIX, VAL_DOUBLE, VAL_DOUBLE, 
			VAL_BASE_ZERO_VERTICAL_BAR, VAL_NO_POINT, VAL_SOLID, 1, VAL_WHITE);
	PlotI=PlotXY(Pnl, PNL_GRAPH, Scan.Nm, Scan.Intensity, NB_PIX, VAL_DOUBLE, VAL_DOUBLE, 
			VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_DK_GRAY);
	
	SetPlotAttribute (Pnl, PNL_GRAPH, PlotI, ATTR_PLOT_SNAPPABLE, 0);
	cb_Display(Pnl, PNL_DISPLAY, EVENT_COMMIT, NULL, 0, 0);
//	SetCtrlAttribute (Pnl, PNL_GRAPH, ATTR_REFRESH_GRAPH, 1);
	
	double NmOfPeak=XOfPeak(iMax);
	SetCtrlVal(Pnl, PNL_PEAK, WavelengthToRGB(NmOfPeak));
	sprintf(Str, "Peak at %.1fnm (corrected+interpolated)", NmOfPeak);
	SetCtrlAttribute (Pnl, PNL_PEAK, ATTR_LABEL_TEXT, Str);
	
	SetCtrlAttribute(Pnl, PNL_SATURATION, ATTR_VISIBLE, NbSaturation>0);
//	if (NmOfPeak>Scan.Nm[0]) SetGraphCursor  (Pnl, PNL_GRAPH, 1, NmOfPeak, Max);
//	PlotIntr = PlotPoint (Pnl, PNL_GRAPH, NmOfPeak, Max, VAL_X, VAL_WHITE);
	RefreshGraph (Pnl, PNL_GRAPH);
	Distribute();
}

///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	char PathName[MAX_PATHNAME_LEN];
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "SpectroVisu.uir", PNL)) < 0)
		return -1;
	EnableDragAndDrop (Pnl);
	cb_Panel(Pnl, EVENT_PANEL_SIZE, NULL, 0, 0);
	DisplayPanel (Pnl);
	
	if (argc>1) ProcessFile(argv[1]), DisplaySpectrum();
	else if (VAL_EXISTING_FILE_SELECTED==FileSelectPopup ("", "*.csv", "*.csv", "Select spectrum",
					 VAL_LOAD_BUTTON, 0, 0, 1, 0, PathName))
		ProcessFile(PathName), DisplaySpectrum();
	
	RunUserInterface ();
	
	DiscardPanel (Pnl);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Panel (int panel, int event, void *callbackData,
						  int eventData1, int eventData2) {
	char **FileList=NULL;
	int Height, Width, Top;
	switch (event) {
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;

		case EVENT_PANEL_SIZE:
			GetPanelAttribute(panel, ATTR_HEIGHT, &Height);
			GetPanelAttribute(panel, ATTR_WIDTH,  &Width);
			GetCtrlAttribute (panel, PNL_GRAPH, ATTR_TOP,   &Top);
			SetCtrlAttribute (panel, PNL_GRAPH, ATTR_HEIGHT, Height-Top);
			SetCtrlAttribute (panel, PNL_GRAPH, ATTR_WIDTH,  Width);
			break;
		
		case  EVENT_FILESDROPPED:
			GetDragAndDropData (&FileList, NULL);
			if (FileList) {
				ProcessFile(FileList[0]);	// just process the 1st one, drop the rest
				DisplaySpectrum();
				int i=0;
				while (FileList[i]!=NULL /*and strlen(FileList[i])>0*/) free(FileList[i++]);
				free(FileList);
			}
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Graph (int panel, int control, int event,
						  void *callbackData, int eventData1, int eventData2) {
	int Index, Plot;
	double X, Y;
	char Str[80], *Intrp="";
	switch (event) {
		case EVENT_COMMIT:
			GetGraphCursor     (panel, control, 1, &X, &Y);
			GetGraphCursorIndex(panel, control, 1, &Plot, &Index);
//			if (PlotIntr) DeleteGraphPlot (panel, control, PlotIntr, VAL_NO_DRAW); PlotIntr=0;
			if (Index>1 and Index<2048-2 and // Do a peak search to find a better X
					Scan.Corrected[Index-2]<Scan.Corrected[Index] and
					Scan.Corrected[Index-1]<Scan.Corrected[Index] and
					Scan.Corrected[Index+1]<Scan.Corrected[Index] and
					Scan.Corrected[Index+2]<Scan.Corrected[Index]) {
				Intrp=" (interpolated)";
				X=XOfPeak(Index);
//				PlotIntr = PlotPoint (panel, control, X, Y, VAL_X, VAL_WHITE);

//				sprintf(Str, "Peak at %.1fnm%s", X, Intrp);
				int Mode;
				double Min, Max;
				GetAxisScalingMode (panel, control, VAL_LEFT_YAXIS, &Mode, &Min, &Max);
				sprintf(Str, "%.1fnm", X);
				AddGraphAnnotation (panel, control, X, Y, Str, 15, Y>(Min+Max/2) ? 15 : -20);
				RefreshGraph (panel, control);
			}
				
			SetCtrlAttribute(panel, PNL_X, ATTR_VISIBLE, 1);
			SetCtrlAttribute(panel, PNL_Y, ATTR_VISIBLE, 1);
			SetCtrlVal(panel, PNL_Y, Y);
			SetCtrlVal(panel, PNL_X, WavelengthToRGB(X));
			sprintf(Str, "Cursor at %.1fnm%s", X, Intrp);
			SetCtrlAttribute (panel, PNL_X, ATTR_LABEL_TEXT, Str);
			Distribute();
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Display (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Disp);
			if (PlotI) SetPlotAttribute (panel, PNL_GRAPH, PlotI, ATTR_TRACE_VISIBLE, (Disp&1)!=0);
			if (PlotC) SetPlotAttribute (panel, PNL_GRAPH, PlotC, ATTR_TRACE_VISIBLE, (Disp&2)!=0);
			break;
	}
	return 0;
}

int CVICALLBACK cb_LogScale (int panel, int control, int event,
							 void *callbackData, int eventData1, int eventData2) {
	int Log;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Log);
			SetCtrlAttribute (panel, PNL_GRAPH, ATTR_YMAP_MODE, Log ? VAL_LOG : VAL_LINEAR);
			break;
	}
	return 0;
}

int CVICALLBACK cb_Lines (int panel, int control, int event,
						  void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Lines);
			DisplaySpectrum();
			break;
	}
	return 0;
}
